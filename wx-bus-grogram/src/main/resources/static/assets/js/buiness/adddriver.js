$(document).ready(function () {
    //性别下拉菜单
    $(document).on("click","li.select.driverGender",function () {
        $("input[name='driverGender']").val($(this).text());
    })
    //驾照类型下拉菜单
    $(document).on("click","li.select.drivingType",function () {
        $("input[name='drivingType']").val($(this).text());
    })
    //司机类型下来菜单
    $(document).on("click","li.select.driverMark",function () {
        $("input[name='driverMark']").val($(this).text());
        $("input[name='driverMark']").data("driverMark",$(this).val());
    })
    $("button.btn.btn-info.btn-fill.btn-wd").click(function () {
        var Driver={};
        Driver.driverCitizenship=$("input[name='driverCitizenship']").val();
        if(Driver.driverCitizenship==""){
            alert("请输入身份证号");
            return false;
        }
        if(eval(Driver.driverCitizenship.length)!=eval(18)){
            alert("请输入正确的身份证号");
            return false;
        }

        Driver.driverName=$("input[name='driverName']").val();
        if(Driver.driverName==""){
            alert("请输入真实姓名");
            return false;
        }

        Driver.driverGender=$("input[name='driverGender']").val();
        if(Driver.driverGender==""){
            alert("请选择性别");
            return false;
        }

        Driver.driverId=$("input[name='driverId']").val();
        if(Driver.driverId==""){
            alert("请输入账号");
            return false;
        }
        if(Driver.driverId.length<8){
            alert("请输入8位以上的账号账号");
            return false;
        }
        Driver.driverPassword=$("input[name='driverPassword']").val();
        if(Driver.driverPassword==""){
            alert("请输入密码!")
            return false;
        }
        if(Driver.driverPassword.length<8){
            alert("请使用更加复杂的密码!")
            return false;
        }

        Driver.licence=$("input[name='licence']").val();
        if(Driver.licence==""){
            alert("请输入驾驶证号码!")
            return false;
        }

        Driver.driverNationality=$("input[name='driverNationality']").val();
        if(Driver.driverNationality==""){
            alert("请输入国籍");
            return false;
        }

        Driver.driverAddress=$("input[name='driverAddress']").val();
        if(Driver.driverAddress==""){
            alert("请输入住址");
            return false;
        }


        Driver.birthday=$("input[name='birthday']").val()+" 00:00:00";
        if(Driver.birthday==""){
            alert("请输入出生日期");
            return false;
        }

        Driver.driverMobile=$("input[name='driverMobile']").val();
        if(Driver.driverMobile==""){
            alert("请输入手机号");
            return false;
        }

        Driver.drivingType=$("input[name='drivingType']").val();
        if(Driver.drivingType==""){
            alert("请输入准驾车型");
            return false;
        }

        Driver.driverMark=$("input[name='driverMark']").data("driverMark");
        if(Driver.driverMark==""){
            alert("请选择类型标识");
            return false;
        }else {
            Driver.driverMark=parseInt( Driver.driverMark,10)-1;
        }

        $.ajax({
            type: 'POST',
            url: 'addDriver',
            contentType: "application/json;charset=utf-8",
            DataType: "json",
            data: JSON.stringify(Driver),
            error: function () {
                alert("加载失败，请刷新重试！");
            },
            success: function (res) {
               if(eval(res.errno)==0){
                   alert("添加成功！");
                   window.location.href="/web/driver";
               }else {
                   alert("添加失败");
               }
            }
        })
    })


})