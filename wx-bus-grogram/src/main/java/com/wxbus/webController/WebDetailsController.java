package com.wxbus.webController;

import com.wxbus.daomain.Passenger;
import com.wxbus.daomain.Route;
import com.wxbus.service.PassengerRouteService;
import com.wxbus.service.RouteService;
import com.wxbus.service.StationService;
import com.wxbus.util.JacksonUtil;
import com.wxbus.util.QRcodeUtil;
import com.wxbus.util.ResponseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/web/details")
public class WebDetailsController {
    private Log log= LogFactory.getLog(WebDetailsController.class);

    @Autowired
    private RouteService routeService;
    @Autowired
    private PassengerRouteService passengerRouteService;
    @Autowired
    private StationService stationService;
    @GetMapping("/examline/{routid}")
    public String findRouteDetailsById(@PathVariable("routid") Integer routid, Model model){
        Route route=routeService.findRouteById(routid);
        String stationId=route.getStationId();
        //截取站点id，搜索 查询站点名称
        String stationNameList="";
        if(!("").equals(stationId)&&stationId!=null){
            String []stationList=stationId.split(",");
            for(int i=0;i<stationList.length;i++){
                if(i<stationList.length-1)
                    stationNameList+=stationService.findStationById(Integer.parseInt(stationList[i])).getStationName()+"、";
                else
                    stationNameList+=stationService.findStationById(Integer.parseInt(stationList[i])).getStationName();
            }
        }
        route.setStationId(stationNameList);
        model.addAttribute("route",route);
        return "routdetails";
    }

    @GetMapping("/updateline/{routid}")
    public String findRouteForUpdate(@PathVariable("routid") Integer routid, Model model){
        Route route=routeService.findRouteById(routid);
        String stationId=route.getStationId();
        //截取站点id，搜索 查询站点名称
        String stationNameList="";
        if(!("").equals(stationId)&&stationId!=null){
            String []stationList=stationId.split(",");
            for(int i=0;i<stationList.length;i++){
                if(i<stationList.length-1)
                    stationNameList+=stationService.findStationById(Integer.parseInt(stationList[i])).getStationName()+"、";
                else
                    stationNameList+=stationService.findStationById(Integer.parseInt(stationList[i])).getStationName();
            }
        }
        route.setStationId(stationNameList);
        model.addAttribute("route",route);
        return "updateroute";

    }
    @GetMapping("/allotbus/{routid}")
    public String allotBus(@PathVariable("routid") Integer routid, Model model){
        Route route=routeService.findRouteById(routid);
        //查询购买数量
        route.setRecruitNum(passengerRouteService.findPassengerCountByRouteId(route.getRouteId()));

        model.addAttribute("route",route);
        return "allotbus";
    }

    @GetMapping("/allotdriver/{routid}")
    public String allotDriver(@PathVariable("routid") Integer routid, Model model){
        Route route=routeService.findRouteById(routid);
        //查询购买数量
        route.setRecruitNum(passengerRouteService.findPassengerCountByRouteId(route.getRouteId()));
        model.addAttribute("route",route);

        return "allotdriver";
    }
    @GetMapping("/busqrcode/{busid}")
    public void busQrcode(@PathVariable("busid") String busid, HttpServletResponse response){

        QRcodeUtil qRcodeUtil=new QRcodeUtil();
        try {
            qRcodeUtil.getTwoDimension(busid,response,500);
        }catch (Exception e){
            log.error("生成二维码异常");
            e.printStackTrace();
        }

    }
    @PostMapping("/postbusqrcode}")
    public void busQrcodePst(@RequestBody String body, HttpServletResponse response){

        String busid=JacksonUtil.parseString(body,"busid");
        QRcodeUtil qRcodeUtil=new QRcodeUtil();
        try {
            qRcodeUtil.getTwoDimension(busid,response,500);
        }catch (Exception e){
            log.error("生成二维码异常");
            e.printStackTrace();
        }

    }
}
